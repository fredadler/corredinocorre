﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStates : MonoBehaviour
{
    bool isRunning;
    public bool IsRunning { get => isRunning; set { isRunning = value; } }
    bool isDead;
    public bool IsDead
    {
        get => isDead;
        set
        {
            isDead = value;
        }
    }

}
