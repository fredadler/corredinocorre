﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Movement/StopRun")]
public class MoveStopRunCommand : Command
{
    public override void Execute(ICommand command, Transform observerTransform)
    {
        if (command.GetType() != this.GetType()) return;

        foreach (MovementRequirements item in movementRequirements)
        {
            if (!item.CanExecute(observerTransform)) return;
        }
        SetRun(observerTransform);
    }

    void SetRun(Transform transformToMove)
    {
        transformToMove.GetComponent<PlayerStates>().IsRunning = false;
    }
}
