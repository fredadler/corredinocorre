﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Movement/MoveStop")]
public class MoveStopCommand : Command
{
    public override void Execute(ICommand command, Transform observerTransform)
    {
        if (command.GetType() != this.GetType()) return;

        foreach (MovementRequirements item in movementRequirements)
        {
            if (!item.CanExecute(observerTransform)) return;
        }
        MovingStop(observerTransform);
    }

    void MovingStop(Transform transformToMove)
    {
        Rigidbody2D targetRigidbody = transformToMove.GetComponent<Rigidbody2D>();
        targetRigidbody.velocity = Vector2.zero;
    }
}