﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Movement/MoveRight")]
public class MoveRightCommand : Command
{
    public override void Execute(ICommand command, Transform observerTransform)
    {
        if (command.GetType() != this.GetType()) return;

        foreach (MovementRequirements item in movementRequirements)
        {
            if (!item.CanExecute(observerTransform)) return;
        }
        MovingRight(observerTransform);
    }

    void MovingRight(Transform transformToMove)
    {
        float runAmountModifier = transformToMove.GetComponent<PlayerStates>().IsRunning ? 2 : 1;
        Rigidbody2D targetRigidbody = transformToMove.GetComponent<Rigidbody2D>();
        targetRigidbody.velocity = new Vector2(amount * runAmountModifier, targetRigidbody.velocity.y);
    }
}
