﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Movement/MoveRequirements/IsOnGround")]
public class MovRequirementIsOnGround : MovementRequirements
{
    public override bool CanExecute(Transform observerTransform)
    {
        return IsOnGround(observerTransform);
    }

    bool IsOnGround(Transform observerTransform)
    {
        return observerTransform.GetComponent<Collider2D>().IsTouchingLayers();
    }
}
