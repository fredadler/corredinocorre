﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Movement/MoveJump")]
public class MoveJumpCommand : Command
{
    public override void Execute(ICommand command, Transform observerTransform)
    {
        if (command.GetType() != this.GetType()) return;

        foreach (MovementRequirements item in movementRequirements)
        {
            if (!item.CanExecute(observerTransform)) return;
        }
        MovingJump(observerTransform);
    }

    void MovingJump(Transform transformToMove)
    {
        float runAmountModifier = transformToMove.GetComponent<PlayerStates>().IsRunning ? 1.2f : 1;
        Rigidbody2D targetRigidbody = transformToMove.GetComponent<Rigidbody2D>();
        targetRigidbody.AddForce(new Vector2(0, amount * runAmountModifier), ForceMode2D.Impulse);
    }
}