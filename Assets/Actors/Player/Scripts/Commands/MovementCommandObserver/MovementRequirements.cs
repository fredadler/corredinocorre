﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovementRequirements : ScriptableObject, ICommandRequirements
{
    public abstract bool CanExecute(Transform observerTransform);
}
