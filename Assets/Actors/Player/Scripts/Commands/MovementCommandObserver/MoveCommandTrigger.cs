﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCommandTrigger : MonoBehaviour
{
    /*
    CONVERTER PARA INJEÇÃO DE DEPENDENCIA SE DER TEMPO
    public InputManager InputManager { get; set; }
    MoveCommandTrigger(InputManager inputManager)
    {
        InputManager = inputManager;
    }
    */

    public Command[] myCommand;

    public void TriggerCommand(int commandIndex)
    {
        InputManager.instance.Notify(myCommand[commandIndex]);
    }
}
