﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PlayerAnimation/Jump")]
public class PlayerAnimationJump : Command
{
    public override void Execute(ICommand command, Transform observerTransform)
    {
        //VALIDADE COMMAND TYPE
        if (command.GetType() != typeof(MoveJumpCommand)) return;

        //VALIDADE COMMAND REQUIREMENTS
        foreach (MovementRequirements item in movementRequirements)
        {
            if (!item.CanExecute(observerTransform)) return;
        }

        //EXECUTE COMMAND ACTION
        Animate(observerTransform);
    }

    void Animate(Transform transformToMove)
    {
        Animator playerAnimator = transformToMove.GetComponentInChildren<Animator>();
        playerAnimator.SetTrigger("Jump");
    }
}
