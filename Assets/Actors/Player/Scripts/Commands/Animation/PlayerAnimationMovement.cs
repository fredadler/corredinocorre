﻿using UnityEngine;

[CreateAssetMenu(menuName = "PlayerAnimation/Movement")]
public class PlayerAnimationMovement : Command
{
    public override void Execute(ICommand command, Transform observerTransform)
    {
        //VALIDADE COMMAND TYPE
        if (command.GetType() != typeof(MoveLeftCommand)
        && command.GetType() != typeof(MoveRightCommand)
        && command.GetType() != typeof(MoveStopCommand)) return;

        //VALIDADE COMMAND REQUIREMENTS
        foreach (MovementRequirements item in movementRequirements)
        {
            if (!item.CanExecute(observerTransform)) return;
        }

        //VERIFY MOVE DIRECION
        float xScale = observerTransform.localScale.x;
        float movementValue = 0;
        if (((Command)command).amount != 0)
        {
            xScale = ((Command)command).amount > 0 ? 1 : -1;

            movementValue = observerTransform.GetComponent<PlayerStates>().IsRunning ? 1 : 0.5f;
        }


        //EXECUTE COMMAND ACTION

        Animate(observerTransform, movementValue);
        observerTransform.localScale = new Vector3(xScale, 1, 1); //SPRITE DO DINO NÃO ESTA PADRONIZADO EM TAMANHO, POR ISSO VAI TER UMA INVERSÃO RUIM
    }

    void Animate(Transform transformToMove, float amount)
    {

        Animator playerAnimator = transformToMove.GetComponentInChildren<Animator>();
        playerAnimator.SetFloat("Move", Mathf.Clamp01(amount));
    }
}