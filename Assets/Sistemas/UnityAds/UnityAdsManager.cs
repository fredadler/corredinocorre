﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAdsManager : MonoBehaviour
{
    private void Awake()
    {
#if UNITY_EDITOR
        Advertisement.Initialize("4392693");
#elif UNITY_ANDROID
        Advertisement.Initialize("4392693");
#elif UNITY_IOS
        Advertisement.Initialize("4392692");
#endif
    }

    public void ShowAds()
    {
#if UNITY_EDITOR
        Advertisement.Show("Interstitial_Android");
#elif UNITY_ANDROID
        Advertisement.Show("Interstitial_Android");
#elif UNITY_IOS
        Advertisement.Show("Interstitial_iOS");
#endif
    }
}