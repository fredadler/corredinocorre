﻿using UnityEngine;
using UnityEngine.Events;

public class CallEventOnTrigger : MonoBehaviour
{
    public UnityEvent triggerEvent;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            triggerEvent.Invoke();
        }
    }


}
