﻿[System.Serializable]
public class TextLocalizationData
{
	public TextLocalizationItem[] items;
}

[System.Serializable]
public class TextLocalizationItem
{
	public string key;
	public string value;
}
