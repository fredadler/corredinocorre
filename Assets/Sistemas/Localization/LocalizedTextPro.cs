﻿using System.Collections;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI)), DisallowMultipleComponent]
public class LocalizedTextPro : MonoBehaviour
{
    [SerializeField] private string key;
    [SerializeField] private TextMeshProUGUI uiTextComponent;

    private void Awake()
    {
        if (uiTextComponent == null)
            uiTextComponent = GetComponent<TextMeshProUGUI>();
        else if (uiTextComponent == null)
            Debug.LogError("LocalizedText lacking UI Text component.");
    }

    private IEnumerator Start()
    {
        TextLocalization.OnLocalizationChange += GetLocalizedValue;

        while (!TextLocalization.IsReady)
            yield return null;

        GetLocalizedValue();
    }

    [ContextMenu("GetLocalizedValue()")]
    private void GetLocalizedValue()
    {
        uiTextComponent.text = TextLocalization.GetLocalizedValue(key);
    }

    private void OnDisable()
    {
        TextLocalization.OnLocalizationChange -= GetLocalizedValue;
    }
}
