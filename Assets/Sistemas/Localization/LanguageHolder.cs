﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageHolder : MonoBehaviour
{
    public void SetLanguage(string language)
    {
        LanguageManager.languageManager.SetLanguage(language);
    }
}
