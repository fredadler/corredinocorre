﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text)), DisallowMultipleComponent]
public class LocalizedText : MonoBehaviour
{
    [SerializeField, Tooltip("Key do dicionario no arquivo Json")] private string key;
    [SerializeField, Tooltip("Não precisa referênciar. Ele procura o Text no mesmo objeto.")] private Text uiTextComponent;

    private void Awake()
    {
        if (uiTextComponent == null)
            uiTextComponent = GetComponent<Text>();
        else if (uiTextComponent == null)
            Debug.LogError("LocalizedText lacking UI Text component.");
    }

    private IEnumerator Start()
    {
        TextLocalization.OnLocalizationChange += GetLocalizedValue;

        while (!TextLocalization.IsReady)
            yield return null;

        GetLocalizedValue();
    }

    private void GetLocalizedValue()
    {
        uiTextComponent.text = TextLocalization.GetLocalizedValue(key);
    }

    private void OnDisable()
    {
        TextLocalization.OnLocalizationChange -= GetLocalizedValue;
    }
}