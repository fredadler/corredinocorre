﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TextLocalization
{
    public static event Action OnLocalizationChange = delegate { };

    public static bool IsReady { get { return isReady; } }

    private static Dictionary<string, string> localizedText;
    private static bool isReady = false;
    private static string missingTextString = "Localized value not found.";

    public static bool LoadLocalizedText(string fileName)
    {
        bool result;

        localizedText = new Dictionary<string, string>();
        string filePath = Path.Combine(Application.streamingAssetsPath+"/Languages", fileName);
        string dataAsJson;

#if UNITY_EDITOR

        dataAsJson = File.ReadAllText(filePath);

#elif UNITY_ANDROID
        
        WWW reader = new WWW(filePath);
            while (!reader.isDone) { }

            dataAsJson = reader.text;
#elif UNITY_IOS
    filePath = Path.Combine(Application.streamingAssetsPath + "/Raw/", fileName);
    dataAsJson = File.ReadAllText(filePath);
#endif

        TextLocalizationData loadedData = JsonUtility.FromJson<TextLocalizationData>(dataAsJson);

        for (int i = 0; i < loadedData.items.Length; i++)
        {
            localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
        }

        result = true;
        isReady = true;
        OnLocalizationChange();

        return result;
    }

    public static string GetLocalizedValue(string key)
    {
        if (!IsReady)
            LoadLocalizedText("localizedText-" + "ptBR" + ".json");

        string value = missingTextString;

        if (localizedText.ContainsKey(key))
            value = localizedText[key];

        return value;
    }
}
