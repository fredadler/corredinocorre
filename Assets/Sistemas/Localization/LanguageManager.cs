﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class LanguageManager : MonoBehaviour
{
    public static LanguageManager languageManager = null;

    private LanguageManager() { }
    private void Awake()
    {
        if (languageManager == null)
        {
            languageManager = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this);
    }
    private IEnumerator Start()
    {
        if (languageManager == null) languageManager = this;
        else Destroy(gameObject);


        SetLanguage("ptBR");

        while (!TextLocalization.IsReady)
        {
            yield return null;
        }
    }

    public void SetLanguage(string language)
    {
        bool result = TextLocalization.LoadLocalizedText("localizedText-" + language + ".json");
    }
}
