﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverScreenControl : MonoBehaviour
{
    public TMP_Text MoedasText;
    public TMP_Text TempoRestanteText;

    IEnumerator Start()
    {
        while (GameManager.Instance == null) yield return null;

        MoedasText.text = GameManager.Instance.score.ToString("00");
        TempoRestanteText.text = GameManager.Instance.gameTime.ToString("00") + " s";
    }

    private void OnDestroy()
    {
        GameManager.Instance.ResetGameManager();
    }
}
