﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    GameManager() { }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    public int score = 0;
    public int Maxscore = 5;
    public int gameTime = 0;
    public void IncreaseScore()
    {
        score++;
        if (score >= Maxscore)
        {
            LevelLoader.Instance.LoadNextLevelAsync();
            UnityAnalyticsManager.CallAnalytics();
        }
    }

    public void ResetGameManager()
    {
        score = 0;
        gameTime = 0;
    }
}
