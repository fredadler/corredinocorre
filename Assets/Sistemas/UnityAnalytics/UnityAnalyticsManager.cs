﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public static class UnityAnalyticsManager
{
    public static void CallAnalytics()
    {
        Analytics.CustomEvent("CollectableQuantity" + GameManager.Instance.score);
        Analytics.CustomEvent("TimeLeft" + GameManager.Instance.gameTime);
    }
}
