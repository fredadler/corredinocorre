﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class MenuWindow : MonoBehaviour
{
    public CanvasGroup MyCanvasGroup { get; set; }

    private void Awake()
    {
        MyCanvasGroup = GetComponent<CanvasGroup>();
    }

}
