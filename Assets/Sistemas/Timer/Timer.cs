﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    public int seconds = 60;
    public TMP_Text textoCount;
    private void OnEnable()
    {
        StartCoroutine(CountDown());
    }
    private void OnDisable()
    {
        GameManager.Instance.gameTime = seconds;
    }

    IEnumerator CountDown()
    {
        WaitForSeconds wait = new WaitForSeconds(1);
        while (seconds > 0)
        {
            yield return wait;
            seconds--;
            textoCount.text = seconds.ToString("00");
        }
        LevelLoader.Instance.LoadNextLevelAsync();
    }
}