﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTestOnEditor : MonoBehaviour
{
    public static int numberOfInputTestOnEditor = 0;
    public Command moverDireita;
    public Command moverEsquerda;
    public Command pular;
    public Command correr;
    public Command paraCorrer;
    public Command parar;

    private void Awake()
    {
        if (numberOfInputTestOnEditor > 0)
        {
            Destroy(gameObject);
        }
        numberOfInputTestOnEditor++;
        DontDestroyOnLoad(gameObject);
    }
    public void MoverDireita()
    {
        InputManager.instance.Notify(moverDireita);
    }
    public void MoverEsquerda()
    {
        InputManager.instance.Notify(moverEsquerda);
    }
    public void Pular()
    {
        InputManager.instance.Notify(pular);
    }
    public void Correr()
    {
        InputManager.instance.Notify(correr);
    }
    public void PararCorrer()
    {
        InputManager.instance.Notify(paraCorrer);
    }

    public void Parar()
    {
        InputManager.instance.Notify(parar);
    }


    private void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            MoverEsquerda();
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            Parar();
        }
        if (Input.GetKey(KeyCode.D))
        {
            MoverDireita();
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            Parar();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Pular();
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            Correr();
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            PararCorrer();
        }
    }
}
