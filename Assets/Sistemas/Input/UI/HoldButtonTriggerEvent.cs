﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HoldButtonTriggerEvent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public UnityEvent PointerDownEvent;
    public UnityEvent holdButtonEvent;
    public UnityEvent PointerUpEvent;
    public void OnPointerDown(PointerEventData eventData)
    {
        PointerDownEvent?.Invoke();
        InvokeRepeating("ButtonDown", 0, 0.01f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        CancelInvoke();
        PointerUpEvent?.Invoke();
    }

    void ButtonDown()
    {
        holdButtonEvent?.Invoke();
    }
}
